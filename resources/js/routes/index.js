import Vue from 'vue';
import VueRouter from 'vue-router';

// layouts
import AdminLayout from '../layouts/AdminLayout'
import FullLayout from '../layouts/FullLayout'

// no need auth pages
import Login from '../pages/Login'

// need auth pages
import Dashboard from '../pages/Dashboard'
import GantanganIndex from '../pages/GantanganIndex'
import GantanganDetail from '../pages/GantanganDetail'

let authRoutes = [
    {
        name: 'dashboard',
        path: '/',
        component: Dashboard
    },
    {
        name: 'gantangan-index',
        path: '/gantangan',
        component: GantanganIndex,
        meta: {}
    },
    {
        name: 'gantangan-detail',
        path: '/gantangan/detail/:id',
        component: GantanganDetail,
        meta: {}
    }
];

const routes = [
    {
        path: '/',
        component: AdminLayout,
        meta: {
            requiresAuth: true
        },
        children: authRoutes
    },
    {
        path: '/',
        name: 'full-page',
        component: FullLayout,
        children: [
            {
                name: 'login',
                path: '/login',
                component: Login
            }
        ]
    },
];

const router = new VueRouter({ 
    mode: 'history', 
    routes: routes
});

export default router