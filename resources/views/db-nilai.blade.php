@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>List Nilai di DB</h4>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#id</td>
                                <td>Nomor Gantangan</td>
                                <td>Nilai</td>
                                <td>Waktu</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($nilai as $x)
                            <tr>
                                <td>{{ $x->id }}</td>
                                <td>{{ $x->nomor }}</td>
                                <td>{{ $x->nilai }}</td>
                                <td>{{ $x->created_at }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="w-100">
                        {{ $nilai->onEachSide(2)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection