@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mb-3">
        <div class="col-10">
            <a href="{{ url('') }}" class="btn btn-primary">Kembali</a>
        </div>
        <div class="col-2">
            <button data-date="{{$tgl}}" class="btn btn-danger delete"><i class="fa fa-trash"></i></button>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>List Gantangan</h4>
                </div>
                <div class="card-body">
                    <h6>{{ date_create($tgl)->format('d F Y') }}</h6>
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>No. Gantangan</td>
                                <td>Total POIN</td>
                                <td>Rincian</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($list_gantangan as $i => $x)
                            <tr>
                                <td>{{ $i +1 }}</td>
                                <td>
                                    Gantangan No. {{ $x->nomor }} <br> Pemilik = <b>{{ $x->pemilik }}</b>
                                </td>
                                <td>
                                    {{ $x->total_poin }}
                                </td>
                                <td>
                                    <a href="{{ url('detail/' . $tgl . '/' . $x->nomor) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $('.delete').click(function(e){
        var date = $(this).data('date');

        console.log(e, $(this).data('date'));
        if(date){
            var cnf = window.confirm('Tindakan ini akan menghapus semua data ditanggal ' + date)
            
            if(cnf){
                window.location.href = '/delete-by-date/' + date
            }
        }
    })
</script>
@endsection