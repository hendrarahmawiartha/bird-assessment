@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mb-3">
        <div class="col-12">
            <a href="{{ url('/') }}" class="btn btn-primary">Kembali</a>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Pakem Penilaian LOVEBIRD</h4>
                </div>
                <div class="card-body">
                    <img src="{{ url('pakem-penilaian-new.jpg') }}" alt="" class="img-fluid rounded mx-auto d-block">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection