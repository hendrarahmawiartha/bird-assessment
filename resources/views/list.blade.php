@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>List Tanggal Perlombaan</h4>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Tanggal Perlombaan</td>
                                <td>Rincian</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($list_gantangan as $i => $x)
                            <tr>
                                <td>{{ $i +1 }}</td>
                                <td>{{ $x->date_formatted }}</td>
                                <td>
                                    <a href="{{ url('lomba/' . $x->tanggal) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection