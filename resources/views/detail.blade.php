@extends('layouts.app')

@section('css')
<style>

</style>
@endsection
@section('content')
<div class="container">
    <div class="row mb-3">
        <div class="col-12">
            <a href="{{ url('lomba/' . $tgl) }}" class="btn btn-primary">Kembali</a>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Detail Gantangan No. {{ $nomor }}</h4> 
                </div>
                <div class="card-body">
                    <table>
                        <thead>
                            <tr>
                                <th>Pemilik</th>
                                <th>:</th>
                                <th>{{ $pemilik }}</th>
                            </tr>
                            <tr>
                                <th>Total Durasi Kicau*</th>
                                <th>:</th>
                                <th>{{ $total_durasi }} detik</th>
                            </tr>
                            <tr>
                                <th>Total POIN</th>
                                <th>:</th>
                                <th>{{ $total_poin }}</th>
                            </tr>
                        </thead>
                    </table>
                    <small>* akumulasi jumlah kicauan yang lebih dari 1 detik</small>
                    <table class="table mt-4">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Waktu</th>
                                <th>Durasi Kicau</th>
                                <th>POIN</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $i => $x)
                            <tr>
                                <th>{{ $i + 1 }}</th>
                                <th>{{ $x['waktu'] }}</td>
                                <td>{{ $x['durasi'] }} detik</td>
                                <td>{{ $x['poin'] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <div>
                                </div>
                            </tfoot>
                        </table>
                        <div class="w-100">
                        {{ $data->onEachSide(2)->links() }}
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection