<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <!-- heading menu -->
                <li class="app-sidebar__heading">Menu Utama</li>
                <!-- /heading menu -->
                <!-- main menu -->
                <li>
                    <a href="{{ url('/') }}" class="{{ Request::is('/') || Request::is('lomba*') ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-rocket"></i>
                        List Gantangan
                    </a>
                </li>
                <li>
                    <a href="{{ url('/pakem-penilaian') }}" class="{{ Request::is('pakem-penilaian') ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-flag"></i>
                        Pakem Penilaian
                    </a>
                </li>
                <!-- /main menu -->
            </ul>
            <hr>
            <div style="text-align: center; margin-top:30px">
            </div>
            <ul class="vertical-nav-menu">
                <li>
                    <a href="{{ url('db-nilai') }}" class="{{ Request::is('db-nilai') || Request::is('db-nilai*') ? 'mm-active' : '' }}">
                        <i class="metismenu-icon pe-7s-rocket"></i>
                        List Nilai DB
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>