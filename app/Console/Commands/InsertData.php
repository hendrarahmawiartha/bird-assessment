<?php

namespace App\Console\Commands;

use App\Models\Nilai;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class InsertData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:insert-data-nilai';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = [
            [
            "timestamp"=>"2021-06-17 14:26:00",
            "nomor"=>1,
            "nilai"=>670
            ],
            [
            "timestamp"=>"2021-06-17 14:26:01",
            "nomor"=>1,
            "nilai"=>650
            ],
            [
            "timestamp"=>"2021-06-17 14:26:02",
            "nomor"=>1,
            "nilai"=>720
            ],
            [
            "timestamp"=>"2021-06-17 14:26:02",
            "nomor"=>1,
            "nilai"=>650
            ],
            [
            "timestamp"=>"2021-06-17 14:26:03",
            "nomor"=>1,
            "nilai"=>766
            ],
            [
            "timestamp"=>"2021-06-17 14:26:03",
            "nomor"=>1,
            "nilai"=>820
            ],
            [
            "timestamp"=>"2021-06-17 14:26:04",
            "nomor"=>1,
            "nilai"=>820
            ],
            [
            "timestamp"=>"2021-06-17 14:26:04",
            "nomor"=>1,
            "nilai"=>571
            ],
            [
            "timestamp"=>"2021-06-17 14:26:05",
            "nomor"=>1,
            "nilai"=>520
            ],
            [
            "timestamp"=>"2021-06-17 14:26:06",
            "nomor"=>1,
            "nilai"=>520
            ],
            [
            "timestamp"=>"2021-06-17 14:26:06",
            "nomor"=>1,
            "nilai"=>540
            ],
            [
            "timestamp"=>"2021-06-17 14:26:07",
            "nomor"=>1,
            "nilai"=>690
            ],
            [
            "timestamp"=>"2021-06-17 14:26:07",
            "nomor"=>1,
            "nilai"=>710
            ],
            [
            "timestamp"=>"2021-06-17 14:26:08",
            "nomor"=>1,
            "nilai"=>800
            ],
            [
            "timestamp"=>"2021-06-17 14:26:09",
            "nomor"=>1,
            "nilai"=>710
            ],
            [
            "timestamp"=>"2021-06-17 14:26:10",
            "nomor"=>1,
            "nilai"=>960
            ],
            [
            "timestamp"=>"2021-06-17 14:26:13",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:13",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:13",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:14",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:14",
            "nomor"=>1,
            "nilai"=>991
            ],
            [
            "timestamp"=>"2021-06-17 14:26:14",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:15",
            "nomor"=>1,
            "nilai"=>990
            ],
            [
            "timestamp"=>"2021-06-17 14:26:15",
            "nomor"=>1,
            "nilai"=>988
            ],
            [
            "timestamp"=>"2021-06-17 14:26:15",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:16",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:16",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:16",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:17",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:17",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:17",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:18",
            "nomor"=>1,
            "nilai"=>970
            ],
            [
            "timestamp"=>"2021-06-17 14:26:18",
            "nomor"=>1,
            "nilai"=>1000
            ],
            [
            "timestamp"=>"2021-06-17 14:26:18",
            "nomor"=>1,
            "nilai"=>998
            ],
            [
            "timestamp"=>"2021-06-17 14:26:19",
            "nomor"=>1,
            "nilai"=>600
            ],
            [
            "timestamp"=>"2021-06-17 14:26:20",
            "nomor"=>1,
            "nilai"=>870
            ],
            [
            "timestamp"=>"2021-06-17 14:26:20",
            "nomor"=>1,
            "nilai"=>890
            ],
            [
            "timestamp"=>"2021-06-17 14:26:21",
            "nomor"=>1,
            "nilai"=>990
            ],
            [
            "timestamp"=>"2021-06-17 14:26:21",
            "nomor"=>1,
            "nilai"=>980
            ],
            [
            "timestamp"=>"2021-06-17 14:26:22",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:22",
            "nomor"=>1,
            "nilai"=>999
            ],
            [
            "timestamp"=>"2021-06-17 14:26:24",
            "nomor"=>1,
            "nilai"=>490
            ],
            [
            "timestamp"=>"2021-06-17 14:26:25",
            "nomor"=>1,
            "nilai"=>500
            ],
            [
            "timestamp"=>"2021-06-17 14:26:26",
            "nomor"=>1,
            "nilai"=>530
            ],
            [
            "timestamp"=>"2021-06-17 14:26:31",
            "nomor"=>1,
            "nilai"=>870
            ],
            [
            "timestamp"=>"2021-06-17 14:26:32",
            "nomor"=>1,
            "nilai"=>550
            ],
            [
            "timestamp"=>"2021-06-17 14:26:35",
            "nomor"=>1,
            "nilai"=>430
            ],
            [
            "timestamp"=>"2021-06-17 14:26:36",
            "nomor"=>1,
            "nilai"=>430
            ],
            [
            "timestamp"=>"2021-06-17 14:26:38",
            "nomor"=>1,
            "nilai"=>660
            ],
            [
            "timestamp"=>"2021-06-17 14:26:43",
            "nomor"=>1,
            "nilai"=>830
            ],
            [
            "timestamp"=>"2021-06-17 14:26:46",
            "nomor"=>1,
            "nilai"=>740
            ],
            [
            "timestamp"=>"2021-06-17 14:26:51",
            "nomor"=>1,
            "nilai"=>880
            ],
            [
            "timestamp"=>"2021-06-17 14:26:51",
            "nomor"=>1,
            "nilai"=>990
            ],
            [
            "timestamp"=>"2021-06-17 14:26:51",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:52",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:52",
            "nomor"=>1,
            "nilai"=>1010
            ],
            [
            "timestamp"=>"2021-06-17 14:26:52",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:53",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:53",
            "nomor"=>1,
            "nilai"=>990
            ],
            [
            "timestamp"=>"2021-06-17 14:26:53",
            "nomor"=>1,
            "nilai"=>1000
            ],
            [
            "timestamp"=>"2021-06-17 14:26:54",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:54",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:54",
            "nomor"=>1,
            "nilai"=>1020
            ],
            [
            "timestamp"=>"2021-06-17 14:26:55",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:26:55",
            "nomor"=>1,
            "nilai"=>1000
            ],
            [
            "timestamp"=>"2021-06-17 14:26:55",
            "nomor"=>1,
            "nilai"=>990
            ],
            [
            "timestamp"=>"2021-06-17 14:27:00",
            "nomor"=>1,
            "nilai"=>530
            ],
            [
            "timestamp"=>"2021-06-17 14:27:00",
            "nomor"=>1,
            "nilai"=>660
            ],
            [
            "timestamp"=>"2021-06-17 14:27:01",
            "nomor"=>1,
            "nilai"=>620
            ],
            [
            "timestamp"=>"2021-06-17 14:27:03",
            "nomor"=>1,
            "nilai"=>780
            ],
            [
            "timestamp"=>"2021-06-17 14:27:09",
            "nomor"=>1,
            "nilai"=>890
            ],
            [
            "timestamp"=>"2021-06-17 14:27:10",
            "nomor"=>1,
            "nilai"=>770
            ],
            [
            "timestamp"=>"2021-06-17 14:27:13",
            "nomor"=>1,
            "nilai"=>920
            ],
            [
            "timestamp"=>"2021-06-17 14:27:13",
            "nomor"=>1,
            "nilai"=>990
            ],
            [
            "timestamp"=>"2021-06-17 14:27:13",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:27:14",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:27:14",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:27:14",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:27:15",
            "nomor"=>1,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:27:15",
            "nomor"=>1,
            "nilai"=>990
            ],
            [
            "timestamp"=>"2021-06-17 14:27:15",
            "nomor"=>1,
            "nilai"=>890
            ],
            [
            "timestamp"=>"2021-06-17 14:27:18",
            "nomor"=>1,
            "nilai"=>690
            ],
            [
            "timestamp"=>"2021-06-17 14:27:19",
            "nomor"=>1,
            "nilai"=>710
            ],
            [
            "timestamp"=>"2021-06-17 14:27:20",
            "nomor"=>1,
            "nilai"=>550
            ],
            [
            "timestamp"=>"2021-06-17 14:27:25",
            "nomor"=>1,
            "nilai"=>670
            ],
            [
            "timestamp"=>"2021-06-17 14:27:35",
            "nomor"=>1,
            "nilai"=>880
            ],
            [
            "timestamp"=>"2021-06-17 14:27:37",
            "nomor"=>1,
            "nilai"=>770
            ],
            [
            "timestamp"=>"2021-06-17 14:27:38",
            "nomor"=>1,
            "nilai"=>750
            ],
            [
            "timestamp"=>"2021-06-17 14:27:40",
            "nomor"=>1,
            "nilai"=>800
            ],
            [
            "timestamp"=>"2021-06-17 14:27:41",
            "nomor"=>1,
            "nilai"=>780
            ],
            [
            "timestamp"=>"2021-06-17 14:27:43",
            "nomor"=>1,
            "nilai"=>820
            ],
            [
            "timestamp"=>"2021-06-17 14:27:45",
            "nomor"=>1,
            "nilai"=>600
            ],
            [
            "timestamp"=>"2021-06-17 14:27:45",
            "nomor"=>1,
            "nilai"=>530
            ],
            [
            "timestamp"=>"2021-06-17 14:27:47",
            "nomor"=>1,
            "nilai"=>580
            ],
            [
            "timestamp"=>"2021-06-17 14:27:48",
            "nomor"=>1,
            "nilai"=>630
            ],
            [
            "timestamp"=>"2021-06-17 14:27:52",
            "nomor"=>1,
            "nilai"=>790
            ],
            [
            "timestamp"=>"2021-06-17 14:27:55",
            "nomor"=>1,
            "nilai"=>700
            ],
            [
            "timestamp"=>"2021-06-17 14:27:57",
            "nomor"=>1,
            "nilai"=>660
            ],
            [
            "timestamp"=>"2021-06-17 14:27:58",
            "nomor"=>1,
            "nilai"=>720
            ],
            [
            "timestamp"=>"2021-06-17 14:27:59",
            "nomor"=>1,
            "nilai"=>770
            ],
            [
            "timestamp"=>"2021-06-17 14:31:00",
            "nomor"=>2,
            "nilai"=>500
            ],
            [
            "timestamp"=>"2021-06-17 14:31:03",
            "nomor"=>2,
            "nilai"=>660
            ],
            [
            "timestamp"=>"2021-06-17 14:31:04",
            "nomor"=>2,
            "nilai"=>600
            ],
            [
            "timestamp"=>"2021-06-17 14:31:09",
            "nomor"=>2,
            "nilai"=>520
            ],
            [
            "timestamp"=>"2021-06-17 14:31:15",
            "nomor"=>2,
            "nilai"=>780
            ],
            [
            "timestamp"=>"2021-06-17 14:31:15",
            "nomor"=>2,
            "nilai"=>620
            ],
            [
            "timestamp"=>"2021-06-17 14:31:16",
            "nomor"=>2,
            "nilai"=>780
            ],
            [
            "timestamp"=>"2021-06-17 14:31:16",
            "nomor"=>2,
            "nilai"=>790
            ],
            [
            "timestamp"=>"2021-06-17 14:31:16",
            "nomor"=>2,
            "nilai"=>820
            ],
            [
            "timestamp"=>"2021-06-17 14:31:17",
            "nomor"=>2,
            "nilai"=>880
            ],
            [
            "timestamp"=>"2021-06-17 14:31:17",
            "nomor"=>2,
            "nilai"=>820
            ],
            [
            "timestamp"=>"2021-06-17 14:31:17",
            "nomor"=>2,
            "nilai"=>900
            ],
            [
            "timestamp"=>"2021-06-17 14:31:18",
            "nomor"=>2,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:31:18",
            "nomor"=>2,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:31:18",
            "nomor"=>2,
            "nilai"=>1000
            ],
            [
            "timestamp"=>"2021-06-17 14:31:19",
            "nomor"=>2,
            "nilai"=>1000
            ],
            [
            "timestamp"=>"2021-06-17 14:31:19",
            "nomor"=>2,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:31:19",
            "nomor"=>2,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:31:20",
            "nomor"=>2,
            "nilai"=>990
            ],
            [
            "timestamp"=>"2021-06-17 14:31:20",
            "nomor"=>2,
            "nilai"=>1000
            ],
            [
            "timestamp"=>"2021-06-17 14:31:20",
            "nomor"=>2,
            "nilai"=>980
            ],
            [
            "timestamp"=>"2021-06-17 14:31:21",
            "nomor"=>2,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:31:21",
            "nomor"=>2,
            "nilai"=>1023
            ],
            [
            "timestamp"=>"2021-06-17 14:31:21",
            "nomor"=>2,
            "nilai"=>1000
            ],
            [
            "timestamp"=>"2021-06-17 14:31:22",
            "nomor"=>2,
            "nilai"=>990
            ],
            [
            "timestamp"=>"2021-06-17 14:31:22",
            "nomor"=>2,
            "nilai"=>1000
            ],
            [
            "timestamp"=>"2021-06-17 14:31:22",
            "nomor"=>2,
            "nilai"=>1010
            ],
            [
            "timestamp"=>"2021-06-17 14:31:27",
            "nomor"=>2,
            "nilai"=>670
            ],
            [
            "timestamp"=>"2021-06-17 14:31:28",
            "nomor"=>2,
            "nilai"=>870
            ],
            [
            "timestamp"=>"2021-06-17 14:31:29",
            "nomor"=>2,
            "nilai"=>800
            ],
            [
            "timestamp"=>"2021-06-17 14:31:31",
            "nomor"=>2,
            "nilai"=>770
            ],
            [
            "timestamp"=>"2021-06-17 14:31:37",
            "nomor"=>2,
            "nilai"=>780
            ],
            [
            "timestamp"=>"2021-06-17 14:31:38",
            "nomor"=>2,
            "nilai"=>890
            ],
            [
            "timestamp"=>"2021-06-17 14:31:38",
            "nomor"=>2,
            "nilai"=>900
            ],
            [
            "timestamp"=>"2021-06-17 14:31:39",
            "nomor"=>2,
            "nilai"=>920
            ],
            [
            "timestamp"=>"2021-06-17 14:31:39",
            "nomor"=>2,
            "nilai"=>950
            ],
            [
            "timestamp"=>"2021-06-17 14:31:52",
            "nomor"=>2,
            "nilai"=>620
            ],
            [
            "timestamp"=>"2021-06-17 14:32:07",
            "nomor"=>2,
            "nilai"=>530
            ],
            [
            "timestamp"=>"2021-06-17 14:32:08",
            "nomor"=>2,
            "nilai"=>600
            ],
            [
            "timestamp"=>"2021-06-17 14:32:09",
            "nomor"=>2,
            "nilai"=>560
            ],
            [
            "timestamp"=>"2021-06-17 14:32:11",
            "nomor"=>2,
            "nilai"=>660
            ],
            [
            "timestamp"=>"2021-06-17 14:32:12",
            "nomor"=>2,
            "nilai"=>670
            ],
            [
            "timestamp"=>"2021-06-17 14:32:12",
            "nomor"=>2,
            "nilai"=>700
            ],
            [
            "timestamp"=>"2021-06-17 14:32:13",
            "nomor"=>2,
            "nilai"=>720
            ],
            [
            "timestamp"=>"2021-06-17 14:32:13",
            "nomor"=>2,
            "nilai"=>690
            ],
            [
            "timestamp"=>"2021-06-17 14:32:14",
            "nomor"=>2,
            "nilai"=>740
            ],
            [
            "timestamp"=>"2021-06-17 14:32:15",
            "nomor"=>2,
            "nilai"=>620
            ],
            [
            "timestamp"=>"2021-06-17 14:32:32",
            "nomor"=>2,
            "nilai"=>700
            ],
            [
            "timestamp"=>"2021-06-17 14:32:35",
            "nomor"=>2,
            "nilai"=>520
            ],
            [
            "timestamp"=>"2021-06-17 14:32:37",
            "nomor"=>2,
            "nilai"=>500
            ],
            [
            "timestamp"=>"2021-06-17 14:32:44",
            "nomor"=>2,
            "nilai"=>500
            ],
            [
            "timestamp"=>"2021-06-17 14:32:45",
            "nomor"=>2,
            "nilai"=>500
            ],
            [
            "timestamp"=>"2021-06-17 14:32:46",
            "nomor"=>2,
            "nilai"=>500
            ],
            [
            "timestamp"=>"2021-06-17 14:32:48",
            "nomor"=>2,
            "nilai"=>520
            ],
            [
            "timestamp"=>"2021-06-17 14:32:53",
            "nomor"=>2,
            "nilai"=>620
            ],
            [
            "timestamp"=>"2021-06-17 14:32:54",
            "nomor"=>2,
            "nilai"=>780
            ],
            [
            "timestamp"=>"2021-06-17 14:32:54",
            "nomor"=>2,
            "nilai"=>820
            ],
            [
            "timestamp"=>"2021-06-17 14:32:54",
            "nomor"=>2,
            "nilai"=>880
            ],
            [
            "timestamp"=>"2021-06-17 14:32:55",
            "nomor"=>2,
            "nilai"=>800
            ],
            [
            "timestamp"=>"2021-06-17 14:32:56",
            "nomor"=>2,
            "nilai"=>850
            ],
            [
            "timestamp"=>"2021-06-17 14:32:58",
            "nomor"=>2,
            "nilai"=>890
            ],
            [
            "timestamp"=>"2021-06-17 14:32:59",
            "nomor"=>2,
            "nilai"=>910
            ],
            [
            "timestamp"=>"2021-06-17 14:36:04",
            "nomor"=>3,
            "nilai"=>600
            ],
            [
            "timestamp"=>"2021-06-17 14:36:08",
            "nomor"=>3,
            "nilai"=>590
            ],
            [
            "timestamp"=>"2021-06-17 14:36:13",
            "nomor"=>3,
            "nilai"=>620
            ],
            [
            "timestamp"=>"2021-06-17 14:36:14",
            "nomor"=>3,
            "nilai"=>610
            ],
            [
            "timestamp"=>"2021-06-17 14:36:20",
            "nomor"=>3,
            "nilai"=>600
            ],
            [
            "timestamp"=>"2021-06-17 14:36:28",
            "nomor"=>3,
            "nilai"=>640
            ],
            [
            "timestamp"=>"2021-06-17 14:36:31",
            "nomor"=>3,
            "nilai"=>510
            ],
            [
            "timestamp"=>"2021-06-17 14:36:32",
            "nomor"=>3,
            "nilai"=>560
            ],
            [
            "timestamp"=>"2021-06-17 14:36:33",
            "nomor"=>3,
            "nilai"=>550
            ],
            [
            "timestamp"=>"2021-06-17 14:36:34",
            "nomor"=>3,
            "nilai"=>560
            ],
            [
            "timestamp"=>"2021-06-17 14:36:35",
            "nomor"=>3,
            "nilai"=>620
            ],
            [
            "timestamp"=>"2021-06-17 14:36:35",
            "nomor"=>3,
            "nilai"=>660
            ],
            [
            "timestamp"=>"2021-06-17 14:36:35",
            "nomor"=>3,
            "nilai"=>700
            ],
            [
            "timestamp"=>"2021-06-17 14:36:36",
            "nomor"=>3,
            "nilai"=>720
            ],
            [
            "timestamp"=>"2021-06-17 14:36:47",
            "nomor"=>3,
            "nilai"=>600
            ],
            [
            "timestamp"=>"2021-06-17 14:36:48",
            "nomor"=>3,
            "nilai"=>780
            ],
            [
            "timestamp"=>"2021-06-17 14:36:48",
            "nomor"=>3,
            "nilai"=>800
            ],
            [
            "timestamp"=>"2021-06-17 14:36:56",
            "nomor"=>3,
            "nilai"=>600
            ],
            [
            "timestamp"=>"2021-06-17 14:36:57",
            "nomor"=>3,
            "nilai"=>650
            ],
            [
            "timestamp"=>"2021-06-17 14:36:58",
            "nomor"=>3,
            "nilai"=>630
            ],
            [
            "timestamp"=>"2021-06-17 14:36:59",
            "nomor"=>3,
            "nilai"=>660
            ],
            [
            "timestamp"=>"2021-06-17 14:37:03",
            "nomor"=>3,
            "nilai"=>500
            ],
            [
            "timestamp"=>"2021-06-17 14:37:05",
            "nomor"=>3,
            "nilai"=>530
            ],
            [
            "timestamp"=>"2021-06-17 14:37:06",
            "nomor"=>3,
            "nilai"=>530
            ],
            [
            "timestamp"=>"2021-06-17 14:37:10",
            "nomor"=>3,
            "nilai"=>550
            ],
            [
            "timestamp"=>"2021-06-17 14:37:10",
            "nomor"=>3,
            "nilai"=>570
            ],
            [
            "timestamp"=>"2021-06-17 14:37:10",
            "nomor"=>3,
            "nilai"=>580
            ],
            [
            "timestamp"=>"2021-06-17 14:37:26",
            "nomor"=>3,
            "nilai"=>500
            ],
            [
            "timestamp"=>"2021-06-17 14:37:42",
            "nomor"=>3,
            "nilai"=>690
            ],
            [
            "timestamp"=>"2021-06-17 14:37:47",
            "nomor"=>3,
            "nilai"=>780
            ],
            [
            "timestamp"=>"2021-06-17 14:37:47",
            "nomor"=>3,
            "nilai"=>790
            ],
            [
            "timestamp"=>"2021-06-17 14:37:51",
            "nomor"=>3,
            "nilai"=>810
            ],
            [
            "timestamp"=>"2021-06-17 14:37:58",
            "nomor"=>3,
            "nilai"=>900
            ],                                          
        ];
        DB::beginTransaction();
        try {
            foreach($data as $x){
                $nilai = new Nilai;
                $nilai->id              = Uuid::uuid4();
                $nilai->nomor           = $x['nomor'] ?? 0;
                $nilai->nilai           = $x['nilai'];
                $nilai->nilai_mentah    = $x['nilai'];
                $nilai->keterangan      = null;
                $nilai->created_at      = $x['timestamp'];
                $nilai->updated_at      = $x['timestamp'];
                $nilai->save();
            }
            DB::commit();
            $this->info('success');
            return "Data successfully inserted";
        } catch (Exception $e) {
            DB::rollBack();
            
            $this->error('failed '. $e->getMessage());
            return "Insert data failed " . $e->getMessage();
        }
    }

    private function insertData()
    {

    }
}
