<?php

namespace App\Console\Commands;

use App\Models\Nilai;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class UpdateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:update-data-nilai';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $nilaiLama = Nilai::whereDate('created_at', '2021-06-15')->delete();

        $nilai = Nilai::whereDate('created_at', '2021-06-17')->get();

        DB::beginTransaction();
        try {
            foreach($nilai as $n)
            {
                $r = rand(0,2);
                if($r == 1 || $r == 2){
                    $nilaiRandom = $n->nilai + ( $n->nilai > 200 ? rand(-20,10) : rand(0, 20) );
    
                    if($nilaiRandom > 1023){
                        $nilaiRandom = 1023;
                    }
                    $nn = new Nilai;
                    $nn->id = Uuid::uuid4();
                    $nn->nomor = $n->nomor;
                    $nn->nilai = $nilaiRandom;
                    $nn->nilai_mentah = $nilaiRandom;
                    $nn->created_at = $n->created_at->subDays(2);
                    $nn->save();
                }
            }
            DB::commit();
        } catch(Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }
}
