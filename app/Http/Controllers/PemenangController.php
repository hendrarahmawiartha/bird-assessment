<?php

namespace App\Http\Controllers;

use App\Models\Nilai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PemenangController extends Controller
{
    public function index(Request $request)
    {
        $data = Nilai::select(DB::raw('DATE(created_at) as tanggal'))->groupBy(DB::raw('DATE(created_at)'))->get();

        if($data->isNotEmpty()){
            $data = $data->map(function($row){
                $row->date_formatted = date_create($row->tanggal)->format('d F Y');
                return $row;
            });
        }
        
        return view('pemenang-index', compact('data'));
    }

    public function detail(Request $request, $tgl)
    {
        // PEMENANG
        // diambil yg lebih dari 1 detik, diakumulasi

        $list_gantangan = Nilai::select('nomor')->whereDate('created_at', $tgl)
                        ->groupBy('nomor')
                        ->orderBy('nomor')
                        ->get();

        return view('pemenang-detail');
    }
}
