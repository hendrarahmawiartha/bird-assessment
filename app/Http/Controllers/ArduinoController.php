<?php

namespace App\Http\Controllers;

use App\Models\Nilai;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class ArduinoController extends Controller
{
    public function insertData(Request $request){
        // return $request->input();

        // foreach($request->value as $v){
            $nilai = new Nilai;
            $nilai->id              = Uuid::uuid4();
            $nilai->nomor           = $request->no ?? 0;
            $nilai->nilai           = $request->value;
            $nilai->nilai_mentah    = $request->value;
            $nilai->keterangan      = null;
            $nilai->save();
        // }
        return 'success';
    }
}
