<?php

namespace App\Http\Controllers;

use App\Models\Nilai;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class NilaiController extends Controller
{
    public function index(Request $request)
    {
        $list_gantangan = Nilai::select(DB::raw('DATE(created_at) as tanggal'))->groupBy(DB::raw('DATE(created_at)'))->get();

        if($list_gantangan->isNotEmpty()){
            $list_gantangan = $list_gantangan->map(function($row){
                $row->date_formatted = date_create($row->tanggal)->format('d F Y');
                return $row;
            });
        }

        return view('list', compact('list_gantangan'));
    }

    public function viewGantangan(Request $request, $tgl)
    {
        $list_gantangan = Nilai::select('nomor')->whereDate('created_at', $tgl)
                        ->distinct()
                        ->orderBy('nomor')
                        ->get();
        if($list_gantangan->isNotEmpty()){
            $list_gantangan = $list_gantangan->map(function($row) use ($tgl){
                $data = Nilai::select()->where('nomor', $row->nomor)
                        ->whereDate('created_at', $tgl)
                        ->orderBy('created_at', 'asc')
                        ->get();

                $arrData = [];
                $pastTime = null;
                if($data->isNotEmpty()){
                    foreach($data as $x){
                        if($x->nilai_mentah >= Nilai::BATAS_NILAI){
                            if($pastTime == $x->created_at || $pastTime == $x->created_at->subSeconds(1) || $pastTime == $x->created_at->subSeconds(2)){
                                if(true){
                                    end($arrData);
                                    $arrData[key($arrData)]['durasi'] = ($arrData[key($arrData)]['durasi'] + 1);
                                    $arrData[key($arrData)]['nilai_mentah'][] = $x->nilai_mentah;
                                }
                            } else {
                                $arrData[] = [
                                    'waktu' => $x->created_at->format('Y-m-d H:i:s'),
                                    'durasi' => $x->nilai_mentah >= Nilai::BATAS_NILAI ? 1 : 0,
                                    'nilai_mentah' => [ $x->nilai_mentah ]
                                ];
                            }
                            $pastTime = $x->created_at;
                        }
                    }
                }
                if(!empty($arrData)){
                    foreach($arrData as $key => $x){
                        $arrData[$key]['poin'] = $this->penilaian($arrData[$key]['durasi']);
                    }
                }

                $row->total_durasi = collect($arrData)->where('durasi', '>', 1)->sum('durasi');
                $row->total_poin = collect($arrData)->where('durasi', '>', 1)->sum('poin');
                $row->pemilik = $this->getName($row->nomor);
                return $row;
            });
        }
        // dd($list_gantangan);
        return view('gantangan', compact('list_gantangan', 'tgl'));
    }
    
    public function detail(Request $request, $tgl, $nomor)
    {
        $data = Nilai::select()->where('nomor', $nomor)
                        ->whereDate('created_at', $tgl)
                        ->orderBy('created_at', 'asc')
                        ->get();
        
        $arrData = [];
        $pastTime = null;
        if($data->isNotEmpty()){
            foreach($data as $x){
                if($x->nilai_mentah >= Nilai::BATAS_NILAI){
                    if($pastTime == $x->created_at || $pastTime == $x->created_at->subSeconds(1) || $pastTime == $x->created_at->subSeconds(2)){
                        // dd($x, 'passed', $arrData, count($arrData));
                        if(true){
                            end($arrData);
                            $arrData[key($arrData)]['durasi'] = ($arrData[key($arrData)]['durasi'] + 1);
                            $arrData[key($arrData)]['nilai_mentah'] = $arrData[key($arrData)]['nilai_mentah'] . ', ' . $x->nilai_mentah;
                            $arrData[key($arrData)]['cr_at'] = $arrData[key($arrData)]['cr_at'] . ', ' . $x->created_at->format('Y-m-d H:i:s');
                        }
                    } else {
                        $arrData[] = [
                            'waktu' => $x->created_at->format('Y-m-d H:i:s'),
                            'durasi' => $x->nilai_mentah >= Nilai::BATAS_NILAI ? 1 : 0,
                            'nilai_mentah' => $x->nilai_mentah,
                            'cr_at' => $x->created_at->format('Y-m-d H:i:s')
                        ];
                    }
                    $pastTime = $x->created_at;
                }
            }
        }
        if(!empty($arrData)){
            foreach($arrData as $key => $x){
                $arrData[$key]['poin'] = $this->penilaian($arrData[$key]['durasi']);
            }
        }
        
        $data = $this->paginate($arrData, 10, $request->page, "/detail/" . $tgl . "/" . $nomor);
        // dd($data);
        $data = $this->paginate($arrData, 10, $request->page, "/detail/" . $tgl . "/" . $nomor);
        // dd($data);
        $total_durasi = collect($arrData)->where('durasi', '>', 1)->sum('durasi');
        $total_poin = collect($arrData)->where('durasi', '>', 1)->sum('poin');
        $pemilik = $this->getName($nomor);

        return view('detail', compact('data', 'nomor', 'tgl', 'pemilik', 'total_durasi', 'total_poin'));
    }

    public function update(Request $request, $id)
    {

    }
    
    public function delete(Request $request, $id)
    {
        $nilai = Nilai::find($id);
    }

    public function deleteByDate(Request $request, $date)
    {
        Nilai::whereDate('created_at', $date)->delete();
        return redirect('/');
    }

    public function paginate($items, $perPage = 15, $page = null, $baseUrl = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? 
                       $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        if ($baseUrl) {
            $lap->setPath($baseUrl);
        }

        return $lap;
    }

    public function getName($nomor)
    {
        $names = array(
            'Handoko',
            'Faris',
            'Ahmad',
            'Haris',
            'Rahman',
            'Fajar',
            'Ali',
            'Agung',
            'Willy',
            'Lukman',
            'Febri',
            'Arif',
            'Mulyono',
            'Dion'
        );

        return $names[$nomor];
    }

    public function penilaian($durasi)
    {
        $poin = 0;
        if($durasi >= 2 && $durasi <= 5){
            // 2 detik - 5 detik
            $poin = 2;
        } elseif($durasi > 5 && $durasi <= 10){
            // 6 detik - 10 detik
            $poin = 10;
        } elseif($durasi > 10 && $durasi <= 15){
            // 11 detik - 15 detik
            $poin = 20;
        } elseif($durasi > 15 && $durasi <= 20){
            // 16 detik - 20 detik
            $poin = 30;
        } elseif($durasi > 20 && $durasi <= 25){
            // 21 detik - 25 detik
            $poin = 50;
        } elseif($durasi > 25 && $durasi <= 30){
            // 26 detik - 30 detik
            $poin = 60;
        } elseif($durasi > 30 && $durasi <= 35){
            // 31 detik - 35 detik
            $poin = 70;
        } elseif($durasi > 35 && $durasi <= 40){
            // 36 detik - 40 detik
            $poin = 90;
        } elseif($durasi > 40 && $durasi <= 45){
            // 41 detik - 45 detik
            $poin = 110;
        } elseif($durasi > 45){
            $poin = 130;
        }
        return $poin;
    }

    public function pakemPenilaian()
    {
        return view('pakem-penilaian');
    }

    public function listNilaiDB(Request $request){
        $nilai = Nilai::orderBy('created_at', 'desc')->paginate(30);

        return view('db-nilai', compact('nilai'));
    }
}
