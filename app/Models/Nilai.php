<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    use HasFactory;

    const BATAS_NILAI = 200;

    protected $table = 'nilai';

    public $incrementing = false;
}
