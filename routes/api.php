<?php

use App\Http\Controllers\ArduinoController;
use App\Http\Controllers\NilaiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/nilai', [NilaiController::class, 'index']);
Route::post('/nilai/save', [NilaiController::class, 'store']);
Route::get('/nilai/detail/{id}', [NilaiController::class, 'detail']);
Route::put('/nilai/update/{id}', [NilaiController::class, 'update']);
Route::delete('/nilai/delete/{id}', [NilaiController::class, 'delete']);

// handling from Arduino
Route::get('/assess', [ArduinoController::class, 'insertData']);