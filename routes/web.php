<?php

use App\Http\Controllers\ArduinoController;
use App\Http\Controllers\NilaiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Ramsey\Uuid\Uuid;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [NilaiController::class, 'index']);
Route::get('/lomba/{tgl}', [NilaiController::class, 'viewGantangan']);
Route::get('/detail/{tgl}/{nomor}', [NilaiController::class, 'detail']);
Route::get('/pakem-penilaian', [NilaiController::class, 'pakemPenilaian']);
Route::get('/db-nilai', [NilaiController::class, 'listNilaiDB']);
Route::get('/delete-by-date/{date}', [NilaiController::class, 'deleteByDate']);

Route::get('/guid', function(Request $request){
    if($request->filled('c')){
        for($i = 0; $i < $request->c; $i++){
            $uuid = Uuid::uuid4();
            echo $uuid;
            echo '<br>';
        }
    } else {
        $uuid = Uuid::uuid4();
        echo $uuid;
    }
});

Route::get('/insert-data-nilai', function(){
    $hasil = Artisan::call('data:insert-data-nilai');
    $hasil = Artisan::output();
    return $hasil;
});

// Route::get('/{any}', function () {
//     return view('app');
// })->where('any', '.*');