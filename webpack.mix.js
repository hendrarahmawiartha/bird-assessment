const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    // .scripts([
    //     // 'resources/js/assets/js/core/jquery.min.js',
    //     // 'resources/js/assets/js/core/popper.min.js',
    //     'resources/js/assets/js/plugins/perfect-scrollbar.jquery.min.js',
    //     'resources/js/assets/js/core/bootstrap-material-design.min.js',
    //     'resources/js/assets/js/plugins/moment.min.js',
    //     // 'resources/js/assets/js/plugins/sweetalert2.js',
    //     'resources/js/assets/js/plugins/jquery.validate.min.js',
    //     // 'resources/js/assets/js/plugins/jquery.bootstrap-wizard.js',
    //     // 'resources/js/assets/js/plugins/bootstrap-selectpicker.js',
    //     // 'resources/js/assets/js/plugins/bootstrap-datetimepicker.min.js',
    //     'resources/js/assets/js/plugins/jquery.dataTables.min.js',
    //     // 'resources/js/assets/js/plugins/bootstrap-tagsinput.js',
    //     'resources/js/assets/js/plugins/jasny-bootstrap.min.js',
    //     'resources/js/assets/js/plugins/fullcalendar.min.js',
    //     // 'resources/js/assets/js/plugins/jquery-jvectormap.js',
    //     'resources/js/assets/js/plugins/nouislider.min.js',
    //     'resources/js/assets/js/plugins/arrive.min.js',
    //     'resources/js/assets/js/core-js/2.4.1/core.js',
    //     'resources/js/assets/js/plugins/chartist.min.js',
    //     'resources/js/assets/js/plugins/bootstrap-notify.js',
    //     // 'resources/js/assets/js/plugins/jquery.tagsinput.js',
    //     'resources/js/assets/js/material-dashboard.js',
    //     // 'resources/js/assets/js/material-dashboard.min.js',
    //     'resources/js/assets/js/material-dashboard.min.js.map',
    // ], 'public/js/hasil_combine.js').version()
    .js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();
